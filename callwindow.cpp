#include "callwindow.h"

CallWindow::CallWindow(CellularRadio * Radio, QWidget *parent) : QWidget(parent), m_Radio(Radio)
{
    /*QVBoxLayout * m_Layout;
    QLabel * m_ElapsedTime;
    QLabel * m_PhoneNumber;
    QPushButton * m_EndCall;*/
    m_Layout = new QVBoxLayout();
    m_ElapsedTime = new QLabel("No Time Elapsed");
    m_EndCall = new QToolButton();
    m_PhoneNumber = new QLabel("Not yet set");
    m_EndCall->setText("End Call");
    connect(m_EndCall, &QToolButton::clicked, this, &CallWindow::EndCall);
    m_Timer = new QTimer;
    connect(m_Timer, &QTimer::timeout, this, &CallWindow::UpdateTimer);
    m_Timer->setInterval(1000);
    m_Layout->addWidget(m_PhoneNumber);
    m_Layout->addWidget(m_ElapsedTime);
    m_Layout->addWidget(m_EndCall);
    this->setLayout(m_Layout);
}

void CallWindow::setPhoneNumber(QString PhoneNumber)
{
    m_PhoneNumber->setText(PhoneNumber);
}

void CallWindow::showEvent(QShowEvent * event)
{
    m_DisplayedTime = 0;
    m_ElapsedTime->setText("0");
    m_Timer->start();
    QWidget::showEvent(event);
}

void CallWindow::EndCall()
{
    m_Radio->EndCall();
    close();
}

void CallWindow::UpdateTimer()
{
    m_DisplayedTime++;
    m_ElapsedTime->setNum(m_DisplayedTime);
}
