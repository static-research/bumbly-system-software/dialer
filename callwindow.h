#ifndef CALLWINDOW_H
#define CALLWINDOW_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QToolButton>
#include <QTimer>

#include "common-library/cellularradio.h"
class CallWindow : public QWidget
{
    Q_OBJECT
public:
    explicit CallWindow(CellularRadio * Radio, QWidget *parent = nullptr);
    void setPhoneNumber(QString PhoneNumber);

signals:

public slots:
    void showEvent(QShowEvent * event);
private slots:
    void EndCall();
    void UpdateTimer();
private:
    QVBoxLayout * m_Layout;
    QLabel * m_ElapsedTime;
    QLabel * m_PhoneNumber;
    QToolButton * m_EndCall;
    CellularRadio * m_Radio;
    QTimer * m_Timer;
    int m_DisplayedTime;
};

#endif // CALLWINDOW_H
