#ifndef INCOMINGCALLWINDOW_H
#define INCOMINGCALLWINDOW_H

#include <QWidget>
#include <QToolButton>
#include <QVBoxLayout>

class IncomingCallWindow : public QWidget
{
    Q_OBJECT
public:
    explicit IncomingCallWindow(QWidget *parent = nullptr);

signals:

public slots:
private:
};

#endif // INCOMINGCALLWINDOW_H
