#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->showFullScreen();
    QSqlDatabase DataBase = QSqlDatabase::addDatabase("QMYSQL");
    /*TODO: Switch to user authentication*/
    DataBase.setHostName("127.0.0.1");
    DataBase.setUserName("root");
    DataBase.setPassword("bumblee");
    DataBase.setDatabaseName("Bumbly");
    if(!DataBase.open())
    {
        QMessageBox::information(this, "No valid Connection", "We didn't connect to the database properly, oops!");
    }
    m_Radio = new CellularRadio("/dev/ttyUSB2", "115200");
    m_AddressBook = new AddressBook();
    connect(m_AddressBook, &AddressBook::ContactAccepted, this, &MainWindow::CallContact);
    this->setCentralWidget(m_AddressBook);
    m_CallWindow = new CallWindow(m_Radio);
}

void MainWindow::CallContact(QString Dest)
{
    QSqlQuery AddressBookQuery;
    AddressBookQuery.prepare("SELECT * FROM Contacts WHERE c_id=" + Dest);
    if(!AddressBookQuery.exec())
    {
        QMessageBox::information(this, "ERROR", AddressBookQuery.lastError().text());
    }
    AddressBookQuery.next();
    Dest = AddressBookQuery.value("phonenumber").toString();
    //QMessageBox::information(this, "We would make a call but it's not hooked up yet", Dest);
    m_Radio->PlaceCall(Dest);
    m_CallWindow->setPhoneNumber(Dest);
    m_CallWindow->showFullScreen();
    //close();
}

MainWindow::~MainWindow()
{

}

