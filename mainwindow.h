#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QMenu>
#include <QToolButton>
#include <QScrollArea>
#include <QObject>
#include <QLabel>

#include "common-library/networkinfo.h"
#include "common-library/TextMessage.hpp"
#include "common-library/addressbook.h"
#include "common-library/cellularradio.h"

#include "callwindow.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    void CallContact(QString Dest);
    ~MainWindow();

private:
    CellularRadio * m_Radio;
    CallWindow * m_CallWindow;
    QVBoxLayout * m_GlobalLayout;
    NetworkInfo * m_NetworkInfo;
    AddressBook * m_AddressBook;
};
#endif // MAINWINDOW_H
